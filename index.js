const express = require('express');
const knexModule = require('knex');

const app = express();
const port = 3000;

app.use(express.static('public'));

const knex = knexModule({
  client: 'mysql',
  connection: {
    host: '3d2.cm9.ca',
    user: 'reader',
    password: 'Tomate123',
    database: 'exam3',
    port: 3306,
  },
});

function form(values, errors = {}) {
  function valueAttribute(name) {
    if (values[name]) return `value="${values[name]}" `;
    return '';
  }

  const errorVid = errors.vid ? '<p class="help is-danger">Le numéro de vendeur est requis!</p>' : '';

  return `<form action="/sales">
    <div class="columns">
      <div class="column">
        <div class="field">
          <label class="label">Numéro du vendeur</label>
          <div class="control">
            <input 
              name="vid" 
              class="input ${errors.vid && 'is-danger'}" 
              type="text" 
              placeholder="Numéro du vendeur"
              ${valueAttribute('vid')}
            />
          </div>
          ${errorVid}
        </div>
      </div>
      <div class="column">
        <div class="field">
          <label class="label">Numéro du mois</label>
          <div class="control">
            <input class="input" type="text" placeholder="entre 1 et 12">
          </div>
        </div>
      </div>
      <div class="column is-narrow">
        <div class="field">
          <label class="label">Actions</label>
          <div class="control">
            <input type="submit" class="button is-primary" />
            <a href="/sales" class="button" >Reset</a>            
          </div>
        </div>
      </div>
    </div>
  </form>`;
}

function html({ mainPart }) {
  return `<!DOCTYPE html>
    <html lang="en">
    <head>
      <meta charset="UTF-8">
      <title>Analyse des ventes</title>
      <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.9.1/css/bulma.min.css">
    </head>
    <body>
      <div class="container">
        ${mainPart}
      </div>
    </body>
  </html>`;
}

app.get('/sales', async (req, res) => {
  const vendorId = req.query.vid;

  const errors = {
    vid: 'vid' in req.query && !req.query.vid,
  };

  let htmlSales = '';
  if (vendorId) {
    const [saleItems] = await knex.raw(`SELECT products.*, SUM(sales.price) AS total
      FROM sales
      LEFT JOIN products ON (products.id = sales.product_id)
      WHERE sales.vendor_id = ${vendorId} 
      GROUP BY sales.product_id
      ORDER BY products.name`);

    const tableRowsHTML = [];
    let yearTotal = 0;
    saleItems.forEach((saleItem) => {
      tableRowsHTML.push(`<tr>
        <td>${saleItem.name}</td>
        <td class="has-text-right">${saleItem.total.toFixed(2)} $</td>
      </tr>`);
      yearTotal += saleItem.total;
    });

    htmlSales = `
      <div>
        Numéro du vendeur: ${vendorId}, total cette année: ${yearTotal.toFixed(2)} $
      </div>
      <table class="table">
        <thead>
          <tr>
            <th>Nom du produit</th>
            <th>Montant vendu</th>
          </tr>
        </thead>
        <tbody>
          ${tableRowsHTML.join('')}
        </tbody>
      </table>`;
  }

  const mainPart = `<h1 class="title">Analyse des ventes</h1>
    ${form({ vid: vendorId }, errors)}
    ${htmlSales}`;

  res.send(html({ mainPart }));
});

app.listen(port, () => {
  console.log(`Cette application web peut maintenant recevoir des requêtes: http://localhost:${port}`);
});
